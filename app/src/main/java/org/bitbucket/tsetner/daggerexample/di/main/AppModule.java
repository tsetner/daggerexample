package org.bitbucket.tsetner.daggerexample.di.main;

import android.app.Application;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tomeg on 15.11.16.
 */

@Module
public class AppModule {
    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return application;
    }

    @Provides
    @Singleton
    @Named("1")
    String provideString1() {
        return "string1";
    }

    @Provides
    @Singleton
    @Named("2")
    String provideString2() {
        return "string2";
    }

}