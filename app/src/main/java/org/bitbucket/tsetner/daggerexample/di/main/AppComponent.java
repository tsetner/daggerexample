package org.bitbucket.tsetner.daggerexample.di.main;

import org.bitbucket.tsetner.daggerexample.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by tomeg on 15.11.16.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
}