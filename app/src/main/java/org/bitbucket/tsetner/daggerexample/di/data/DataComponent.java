package org.bitbucket.tsetner.daggerexample.di.data;

import org.bitbucket.tsetner.daggerexample.MainActivity;
import org.bitbucket.tsetner.daggerexample.di.main.AppModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by tomeg on 15.11.16.
 */
@Singleton
@Component(modules = {DataModule.class, AppModule.class})
public interface DataComponent {
    void inject(MainActivity activity);
}
