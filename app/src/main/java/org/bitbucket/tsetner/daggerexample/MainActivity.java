package org.bitbucket.tsetner.daggerexample;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import org.bitbucket.tsetner.daggerexample.di.data.DaggerDataComponent;
import org.bitbucket.tsetner.daggerexample.di.data.DataModule;
import org.bitbucket.tsetner.daggerexample.di.main.AppModule;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;

import dagger.Lazy;

public class MainActivity extends AppCompatActivity {

    @Inject
    Lazy<SharedPreferences>
            sharedPreferences;

    @Inject
    @Named("1")
    String string1;

    @Inject
    @Named("2")
    Provider<String> string2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DaggerDataComponent.builder()
                .appModule(new AppModule(getApplication()))
                .dataModule(new DataModule())
                .build()
                .inject(this);


        Toast.makeText(this, sharedPreferences.get().getString("not exists", string1 + string2.get()), Toast.LENGTH_LONG).show();
    }
}
